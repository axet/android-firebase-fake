# Firebase fake library

Allows you to complie your open-source code with empty firebase libraries. Will help you to run app without any propriatary code and no source code changes. Help to build f-droid builds.

# Java

To check if you are using FirebaseFake library or original one, use following code:

```java

public class MyApplication extends Application {

    public static boolean firebaseEnabled(Context context) {
        ComponentName name = new ComponentName(context, "com.google.firebase.provider.FirebaseInitProvider");
        if (GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context) != ConnectionResult.SUCCESS)
            return false;
        PackageManager pm = context.getPackageManager();
        if (pm.getComponentEnabledSetting(name) == PackageManager.COMPONENT_ENABLED_STATE_DISABLED)
            return false;
        try {
            if (!pm.getProviderInfo(name, PackageManager.MATCH_DEFAULT_ONLY).enabled)
                return false;
        } catch (PackageManager.NameNotFoundException ignore) {
            return false;
        }
        return true;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        boolean detectedFirebase_10_2 = false;
        try {
            FirebaseDatabase.class.getMethod("getInstance", String.class); // new method added FirebaseDatabase.getInstance(String url)
            detectedFirebase_10_2 = true;
        } catch (NoSuchMethodException e) {
        }
        if (Build.VERSION.SDK_INT < 14 && detectedFirebase_10_2) { // disable firebase for API14< and firebase10.2+
            ComponentName name = new ComponentName(base, "com.google.firebase.provider.FirebaseInitProvider");
            PackageManager pm = getPackageManager();
            pm.setComponentEnabledSetting(name, PackageManager.COMPONENT_ENABLED_STATE_DISABLED, 0);
        } else {
            // FirebaseApp.initializeApp(this); // to make setLogLevel call works
            // FirebaseDatabase.getInstance().setLogLevel(Logger.Level.DEBUG);
        }
    }
 }

```
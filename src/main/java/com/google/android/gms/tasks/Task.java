package com.google.android.gms.tasks;

public class Task<T> {

    public Task<T> addOnSuccessListener(OnSuccessListener<T> l) {
        return this;
    }

    public Task<T> addOnFailureListener(OnFailureListener t) {
        return this;
    }

}
